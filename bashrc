# if not running interactively, exit
[ -z "$PS1" ] && return

# do not put duplicate lines in history
HISTCONTROL=ignoredups:ignorespace

# set history length
HISTSIZE=1000
HISTFILESIZE=2000

# append to history file, do not overwrite
shopt -s histappend

# check the window size after each command
shopt -s checkwinsize

# source bash_completion
if [ -f /opt/local/etc/profile.d/bash_completion.sh ]; then
	. /opt/local/etc/profile.d/bash_completion.sh
fi

# setup .inputrc
export INPUTRC=$HOME/.inputrc

# set default editor to vim
if [ -n `which vim` ]; then
	alias vi='vim'
	export EDITOR=vim
	export VISUAL=vim
	export GIT_EDITOR=vim
	export SVN_EDITOR=vim
else
	export EDITOR=vi
	export VISUAL=vi
	export GIT_EDITOR=vi
	export SVN_EDITOR=vi
fi

# set colors for ls
OSNAME=`uname`
if [ "$OSNAME" = "Linux" ]; then
	alias ls='ls --color=auto'
elif [ "$OSNAME" = "Darwin" ] || [ "$OSNAME" = "FreeBSD" ]; then
	alias ls='ls -G'
fi

# automatic ls after cd
cdl()
{
	if [ -n "$1" ]; then
		builtin cd "$@" && ls
	else
		builtin cd $HOME && ls
	fi
}

# set PS1
if [ -f /opt/lib/git-prompt.sh ]; then
	. /opt/lib/git-prompt.sh
	GIT_PS1_SHOWCOLORHINTS=true
	GIT_PS1_SHOWDIRTYSTATE=true
	GIT_PS1_SHOWUNTRACKEDFILES=true
	PROMPT_COMMAND='__git_ps1 "\[\e]0;\u@\h: \w\a\][\u@\h \W" "]\$ "'
else
	PS1='\[\e]0;\u@\h: \w\a\][\u@\h \W]\$ '
fi

# bash aliases
alias l='ls -l'
alias la='ls -A'
alias ll='ls -alF'
alias llh='ls -alFh'
alias lls='ls -alFSr'
alias llsh='ls -alFhSr'
alias cd='cdl'
alias df='df -h'
alias du='du -hc'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
[ -n `which gem` ] && alias remove_all_gems='gem list | cut -d" " -f1 | xargs gem uninstall -aIx'
[ -n `which pkill` ] && alias logout_user='pkill -KILL -u'

# add /opt to the PATH
export PATH=$PATH:/opt/bin:/opt/local/bin:/opt/local/sbin

# set up RVM if it's installed
if [ -e $HOME/.rvm ]; then
	# load RVM into a shell session *as a function*
	[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

	# add rvm to the PATH for scripting
	export PATH=$PATH:$HOME/.rvm/bin
fi

# set up Heroku Toolbelt if it's installed
if [ -e /usr/local/heroku ]; then
	export PATH=/usr/local/heroku/bin:$PATH
fi
